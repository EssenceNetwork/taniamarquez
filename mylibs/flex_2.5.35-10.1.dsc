-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: flex
Binary: flex, flex-doc
Architecture: any all
Version: 2.5.35-10.1
Maintainer: Manoj Srivastava <srivasta@debian.org>
Homepage: http://flex.sf.net/
Standards-Version: 3.8.3.0
Vcs-Browser: http://git.debian.org/?p=users/srivasta/debian/flex.git
Vcs-Git: git://git.debian.org/~srivasta/debian/flex.git
Build-Depends: bison, gettext, texinfo, help2man, file, po-debconf, autoconf, automake | automaken, autopoint
Package-List: 
 flex deb devel optional
 flex-doc deb doc optional
Checksums-Sha1: 
 333c876a8e24ae5a17d9573459fc501b7721930b 1456620 flex_2.5.35.orig.tar.gz
 05a6b0c4536e5bde1d955c489c1a821cb4de0a83 41229 flex_2.5.35-10.1.diff.gz
Checksums-Sha256: 
 a6fe3ac80b5f89769d833efde712f95bb0255bcf9089fa324636a9b8a005c717 1456620 flex_2.5.35.orig.tar.gz
 98f1c978f0f2bd07672d47fadc0219911a2c74727d32f7654cc2885d112e8195 41229 flex_2.5.35-10.1.diff.gz
Files: 
 201d3f38758d95436cbc64903386de0b 1456620 flex_2.5.35.orig.tar.gz
 c2907cbd8ff8ca6adb9694d462caa897 41229 flex_2.5.35-10.1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iEYEARECAAYFAk/vFrUACgkQuW9ciZ2SjJuR/ACeNaqXS1tqx9sQrzjuUKiOW9M9
rH0An0ccMhmkUTCTYRSU/6OcRqZq97hi
=SHur
-----END PGP SIGNATURE-----
