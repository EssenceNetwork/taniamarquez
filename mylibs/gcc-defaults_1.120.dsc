-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: gcc-defaults
Binary: cpp, g++, g++-multilib, gobjc, gobjc-multilib, gobjc++, gobjc++-multilib, gfortran, gfortran-multilib, gccgo, gccgo-multilib, libgcj-common, libgcj-bc, gcj-jdk, gcj-jre-headless, gcj-jre, gcc, gcc-multilib, gdc, gdc-v1, gcc-spu, g++-spu, gfortran-spu
Architecture: any all
Version: 1.120
Maintainer: Debian GCC Maintainers <debian-gcc@lists.debian.org>
Uploaders: Matthias Klose <doko@debian.org>
Standards-Version: 3.9.3
Vcs-Browser: http://svn.debian.org/viewsvn/gcccvs/branches/sid/gcc-defaults
Vcs-Svn: svn+ssh://svn.debian.org/svn/gcccvs/branches/sid/gcc-defaults
Build-Depends: m4, debhelper (>= 5), dpkg-dev (>= 1.16.0~ubuntu4), gcj-4.7-base (>= 4.7.0~) [!arm !m68k], gcc-4.6-base (>= 4.6.1~) [!m68k], gcc-4.7-base [amd64 i386], lsb-release
Build-Depends-Indep: ca-certificates, gcj-jdk, python (>= 2.6.6)
Package-List: 
 cpp deb interpreters optional
 g++ deb devel optional
 g++-multilib deb devel optional
 g++-spu deb devel optional
 gcc deb devel optional
 gcc-multilib deb devel optional
 gcc-spu deb devel optional
 gccgo deb devel optional
 gccgo-multilib deb devel optional
 gcj-jdk deb java optional
 gcj-jre deb java optional
 gcj-jre-headless deb java optional
 gdc deb devel optional
 gdc-v1 deb devel optional
 gfortran deb devel optional
 gfortran-multilib deb devel optional
 gfortran-spu deb devel optional
 gobjc deb devel optional
 gobjc++ deb devel optional
 gobjc++-multilib deb devel optional
 gobjc-multilib deb devel optional
 libgcj-bc deb java optional
 libgcj-common deb java optional
Checksums-Sha1: 
 1f6574b5dee7b57848ebbc3eca2d710f9b43d807 60672 gcc-defaults_1.120.tar.gz
Checksums-Sha256: 
 499d4e061ca09e61e508f52357c3401fa44c727b489a86a893bf5c4f09bb9686 60672 gcc-defaults_1.120.tar.gz
Files: 
 ae16d4d90bd525affe288614a92253f0 60672 gcc-defaults_1.120.tar.gz
Python-Version: all

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)

iEYEARECAAYFAlBjnYAACgkQStlRaw+TLJzZPQCeLSFhKaBz6p0i37vDwCbou0/7
SuQAoJFGkz21HKmvjiF731H6O1II8YOM
=zDU9
-----END PGP SIGNATURE-----
