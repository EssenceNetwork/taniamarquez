-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: bison
Binary: bison, libbison-dev
Architecture: any
Version: 1:2.5.dfsg-2.1
Maintainer: Chuan-kai Lin <cklin@debian.org>
Homepage: http://www.gnu.org/software/bison/
Standards-Version: 3.9.2.0
Build-Depends: debhelper (>= 8.1.3), gettext, m4 (>= 1.4-14), autotools-dev, cdbs (>= 0.4.93)
Package-List: 
 bison deb devel optional
 libbison-dev deb libdevel optional
Checksums-Sha1: 
 bd8121f2b02efbf2ea246ed541343ec675f8477e 1793123 bison_2.5.dfsg.orig.tar.bz2
 2c38cbfe310f86e15dc5b17b7e0f7a7429419e00 8289 bison_2.5.dfsg-2.1.debian.tar.gz
Checksums-Sha256: 
 0be4cc15cb1999d0aa13df84dc61c78b9a1e4fb19fba83cea25544e4c35712b9 1793123 bison_2.5.dfsg.orig.tar.bz2
 15b987d138a5cf677af0c97fd40dc110d43a45cc80770bc58d0170f6e8c24e0a 8289 bison_2.5.dfsg-2.1.debian.tar.gz
Files: 
 166d21db79310d853b738ed1ae7ad11b 1793123 bison_2.5.dfsg.orig.tar.bz2
 f5bb71f8f131623db0b52494da866bba 8289 bison_2.5.dfsg-2.1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)
Comment: Signed by Raphael Hertzog

iQIcBAEBCAAGBQJOz3CWAAoJEOYZBF3yrHKaRUIP+wVV756baPjIS/SLIKItfCzQ
bExKXrvxlLX2OzSwPNXOUADuvqWxuFB8aysASWUsxDitKz9r7LqWrbSJmjyYaN1J
X5NQJlBGKdeckYvM9V1sbqzPvMF/Lfb9zs8TQROmNXFReSCIzVLGZN5xMZ0b87JT
mYW1+47+IKU5GZd6mWQvKgnUsCwjC2Dl6jTDJZD881EBM/j/I4BvyiRM3JIRjIpg
Nrmi3btAnxRtaxWMppI9S+yDzo//8M7sax1QjWYha7z0rOs50LHAdeHUvk7MW8mB
626AjiL4bqgU4WRPDdx2E0lLhrmwTl/x/47LaGU8UdtJlDynvS9YZ79JyIkm/BPc
JO9gzO1AdqGeFbzaM6SFlh03C0fQsbehusOiWkyfYStZ8JZIjEAkNnecDzyyysvG
BZsmKIxTcwlWtjOjNcZAbK7yZwMsMITKf/IIbzFANi67VkCIvIQkHMB4kacEpVLt
VRn+wBvTGRy1HeiU73qfYinxwgP38AJ6uxBv6twNv03bHgp+tI7fG7IPy0+mC54R
IhFp4VnoJw+cN7sT8dlyEfV+BOAo+GFalN3Nhty5gD2UDXjCbnuvWKx6lCDwE+M+
rRl22biNtkzNhTxcqlknYH1sqcvJXqOAGzP/yX1LnInXcqqiPMD5IX3+Jk/GY+fA
xhenOIZBuIWdi9thzcyR
=KfOY
-----END PGP SIGNATURE-----
