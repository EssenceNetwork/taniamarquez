

/*
 * THIS SOURCE CODE IS SUPPLIED  ``AS IS'' WITHOUT WARRANTY OF ANY KIND, 
 * AND ITS AUTHOR AND THE JOURNAL OF ARTIFICIAL INTELLIGENCE RESEARCH 
 * (JAIR) AND JAIR'S PUBLISHERS AND DISTRIBUTORS, DISCLAIM ANY AND ALL 
 * WARRANTIES, INCLUDING BUT NOT LIMITED TO ANY IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND
 * ANY WARRANTIES OR NON INFRINGEMENT.  THE USER ASSUMES ALL LIABILITY AND
 * RESPONSIBILITY FOR USE OF THIS SOURCE CODE, AND NEITHER THE AUTHOR NOR
 * JAIR, NOR JAIR'S PUBLISHERS AND DISTRIBUTORS, WILL BE LIABLE FOR 
 * DAMAGES OF ANY KIND RESULTING FROM ITS USE.  Without limiting the 
 * generality of the foregoing, neither the author, nor JAIR, nor JAIR's
 * publishers and distributors, warrant that the Source Code will be 
 * error-free, will operate without interruption, or will meet the needs 
 * of the user.
 */









/*********************************************************************
 * File: inst_final.h
 * Description: headers for final domain representation functions
 *
 *
 * Author: Joerg Hoffmann 2000
 *
 *********************************************************************/ 







#ifndef _INST_FINAL_H
#define _INST_FINAL_H



void perform_reachability_analysis( void );
int fact_adress( void );
void make_name_inst_table_from_NormOperator( Action *a, NormOperator *o, EasyTemplate *t );
void make_name_inst_table_from_PseudoAction( Action *a, PseudoAction *pa );



void collect_relevant_facts( void );
void create_final_goal_state( void );
void set_relevants_in_wff( WffNode **w );
void create_final_initial_state( void );
void create_final_actions( void );



void build_connectivity_graph( void );
Bool belongs_to_our_class( void );


int constant_out_of_action ( Action * a ); /*CHANGE*/
void create_communicative_actions_out_of_action (int action_instance); /*CHANGE*/ 
void create_communicative_actions_out_of_fluent (int fluent_instance); /*CHANGE*/
void add_to_relevant_facts(Action * temp_a, int i, int adr, int temp, int add_precond); /*CHANGE*/
void update_equivalences(int type, int args0, int args1); /*CHANGE*/
int constant_out_of_fluent(int lp, int * largs); /*CHANGE*/
int extract_fluent (int fluent_position, int * largs); /*CHANGE*/
int is_unknown_fluent (int lp, int * largs); /*CHANGE*/
int is_in_Facts (int lp, int * largs, Facts * facts); /*CHANGE*/

#endif /* _INST_FINAL_H */
