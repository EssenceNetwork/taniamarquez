In this Folder you can find the latest version of a modified Contingent-FF for solving First-Person Communication Planning (FP-CP) problem as defined in the paper entitled "First-Person Planning with Communication in Multiagent Settings".
Currently it supports two types of speech acts: request actions (an agent requests a commitment to perform an action from another agent)  and boolean questions (an agent inquires with another agent if the value of a certain predicate is true or not).

CREATING THE PDDL/DOMAIN REPRESENTATION:
As in any planner you need to have a domain file and facts file, and you will also need to have multiple agents of the type agent in your domain. 
For using the request action, you need to identify actions that need commitment with a predicate (me ?a), and to add the following code to the domain file:

PREDICATES:
(me ?x - agent)
(commits ?x - agent ?ac - act)

ACTIONS:
(:action request_action
:communication
:parameters ( ?a - agent ?b - agent ?ac - act)
:precondition (and (not (= ?a ?b))
(me ?a)
)
:observe (commits ?b ?ac)
)

In the facts file you need to indicate who is the planning agent and the agents you cna request the action to. For that, you need to add these grounded predicates with the relevant agents (in the example we assume agent0 is the planning agent and agent1 is the agent to whom we will request the actions):

(me agent0)
(unknown (commits agent1 act))

For using the boolean question you will need to add the following code to the domain file:

ACTIONS:
(:action boolean_question
:communication
:parameters ( ?a - agent ?b - agent ?fl - fluent)
:precondition (and (not (= ?a ?b))
(me ?a)
)
:observe (fl ?fl)
)

In the facts file you need to indicate which fact you are unsure of. In the following example, we assume that we do not know which agent is in position s0, so the unkwnon instance is only represented with the type of object "agent", instead of by a concrete agent.

(unknown (at-agent agent s0))

RUNNING THE PLANNER:

After writing the domain and facts file, running the planner is simple. You only have to indicate the domain and facts file as follows:

./ff -o domain.pddl -f facts.pddl (OPTIONAL: -I [-g number] -c [1|2|3])

It is unlikely that you will have a solution available for all branches, so you should also run the command with the flag -I, so the planner knows that it needs to stop the search after a certain horizon. By defualt the horizon is 1000, but you can change it by using the flag -g horizon.
The planner also has some communicative heuristics implemented that you can access with -c number_heuristic. By default, the planner will only run with the normal Contingent-FF heuristics.
The available communicative heuristics are as follows:
c = 1: the planner does not consider possible using the communicative action more than once (It was discovered that it did not work, because the planner was already eliminating this actions by defult)
c = 2: the planner considers that the agent is looking more favourable to doing less actions by itself and that communication is "free". It will take longer, but find usually more favourable solutions to the planning agent.

RUNNING AN EXAMPLE:

In the folder testset, there are several examples that you can run to try the planner. For instance, you can use the following command:

./ff -o testset/satellite/domain.pddl -f testset/satellite/p04-pfile4-4.pddl -I -g 5


