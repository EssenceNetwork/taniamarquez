(define (domain rover)
(:requirements :typing)
(:types agent act lander mode camera waypoint)
(:predicates
	 (at ?x ?y)
	 (at_lander ?x ?y) 
	 (can_traverse ?r ?x ?y) 
	 (equipped_for_imaging ?r) 
	 (calibrated ?c ?r) 
	 (supports ?c ?m) 
	 (available ?r) 
	 (visible ?w ?p) 
	 (have_image ?r ?m) 
	 (communicated_image_data ?w ?m) 
	 (visible_from ?o ?w) 
	 (calibration_target ?i ?o) 
	 (on_board ?i ?r) 
	 (channel_free ?l) 
	 (me ?x)
	 (commits ?x - agent ?ac - act)
	 (can_take_image ?x - agent ?w ?m)
)

(:action navigate
 :parameters ( ?x - agent ?y - waypoint ?z - waypoint)
 :precondition
	(and (can_traverse ?x ?y ?z) (available ?x) (at ?x ?y) (visible ?y ?z))
 :effect
	(and (at ?x ?z) (not (at ?x ?y)) (not (can_traverse ?x ?y ?z))))

(:action calibrate
 :parameters ( ?r - agent ?i - camera ?w - waypoint)
 :precondition
	(and (equipped_for_imaging ?r) (calibration_target ?i ?w) (at ?r ?w) (visible_from ?i ?w) (on_board ?i ?r))
 :effect
	 (calibrated ?i ?r))

(:action take_image
 :parameters ( ?r - agent ?p - waypoint ?i - camera ?m - mode)
 :precondition
	(and  (me ?r) (calibrated ?i ?r) (at ?r ?p) (on_board ?i ?r) (equipped_for_imaging ?r) (supports ?i ?m))
 :effect
	(and (have_image ?r ?m) (not (calibrated ?i ?r))))

(:action communicate_image_data
 :parameters ( ?r - agent ?l - lander ?m - mode ?x - waypoint ?y - waypoint)
 :precondition
	(and (at ?r ?x) (at_lander ?l ?y) (have_image ?r ?m) (visible ?x ?y) (available ?r) (channel_free ?l))
 :effect
	(and (channel_free ?l) (communicated_image_data ?x ?m) (available ?r) (not (available ?r)) (not (channel_free ?l))))

(:action request
:communication
:parameters ( ?a - agent ?b - agent ?ac - act)
:precondition (and (not (= ?a ?b))
(me ?a)
)
:observe (commits ?b ?ac)
)

)
