(define (problem ZTRAVEL-1-2)
(:domain zeno-travel)
(:objects
	agent0 agent1 agent2 - agent
	city0 city1 - city
	fl0 fl1 fl2 - flevel
	)
(:init
	(at agent1 city0)
	(aircraft agent1)
	(person agent0)
	(person agent2)
	(at agent2 city0)
	(fuel-level agent1 fl2)
	(at agent0 city0)
	(next fl0 fl1)
	(next fl1 fl2)
	(me agent0)

	(unknown (commits agent1 act))
	(unknown (commits agent2 act))
)
(:goal (and
	(at agent0 city1)
	(at agent2 city1)
	))

)
