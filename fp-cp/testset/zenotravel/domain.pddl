(define (domain zeno-travel)
(:requirements  :typing)
(:types agent city flevel act)
(:predicates
	 (at ?x - agent ?c - city) 
	 (in ?p - agent ?a - agent) 
	 (fuel-level ?a - agent ?l - flevel) 
 	 (next ?l1 - flevel ?l2 - flevel)
	 (aircraft ?p) 
	 (person ?x)
	 (me ?x - agent) 
 	 (commits ?x - agent ?ac - act))

(:action board
 :parameters ( ?p - agent ?a - agent ?c - city)
 :precondition
	(and (person ?p) (aircraft ?a)  (at ?p ?c) (at ?a ?c))
 :effect
	(and (in ?p ?a) (not (at ?p ?c))))

(:action debark
 :parameters ( ?p - agent ?a - agent ?c - city)
 :precondition
	(and (person ?p) (aircraft ?a) (in ?p ?a) (at ?a ?c))
 :effect
	(and (at ?p ?c) (not (in ?p ?a))))

(:action zoom
 :parameters ( ?a - agent ?c1 - city ?c2 - city ?l1 - flevel ?l2 - flevel ?l3 - flevel)
 :precondition
	(and (me ?a) (not (= ?c1 ?c2)) (aircraft ?a) (at ?a ?c1) (fuel-level ?a ?l1) (next ?l2 ?l1) (next ?l3 ?l2))
 :effect
	(and (at ?a ?c2) (fuel-level ?a ?l3) (not (at ?a ?c1)) (not (fuel-level ?a ?l1))))

(:action refuel
 :parameters ( ?a - agent ?c - city ?l - flevel ?l1 - flevel)
 :precondition
	(and (aircraft ?a) (fuel-level ?a ?l) (next ?l ?l1) (at ?a ?c))
 :effect
	(and (fuel-level ?a ?l1) (not (fuel-level ?a ?l))))

(:action request
:communication
:parameters ( ?a - agent ?b - agent ?ac - act)
:precondition (and (not (= ?a ?b))
(me ?a)
)
:observe (commits ?b ?ac)
)

)
