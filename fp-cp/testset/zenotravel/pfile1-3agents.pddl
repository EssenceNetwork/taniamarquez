(define (problem ZTRAVEL-1-2)
(:domain zeno-travel)
(:objects
	agent0 agent1 agent2 agent3 - agent
	city0 city1 - city
	fl0 fl1 fl2 - flevel
	)
(:init
	(at agent1 city0)
	(aircraft agent1)
	(person agent0)
	(aircraft agent2)
	(at agent2 city0)
	(fuel-level agent2 fl2)
	(aircraft agent3)
	(at agent3 city0)
	(fuel-level agent3 fl2)
	(at agent0 city0)
	(next fl0 fl1)
	(next fl1 fl2)
	(me agent0)

	(unknown (commits agent1 act))
	(unknown (commits agent2 act))
	(unknown (commits agent3 act))
)
(:goal (and (at agent0 city1)))

)
