(define (domain satellite)
(:requirements  :typing)
(:types agent act instrument mode direction)
(:predicates
	 (on_board ?i -instrument ?s - agent)
   	(supports ?i -instrument ?m - mode) 
	(pointing ?s - agent ?d - direction) 
	(power_avail ?s - agent) 
	(power_on ?i - instrument) 
	(calibrated ?i - instrument) 
	(have_image ?d - direction ?m - mode) 
	(calibration_target ?i -instrument ?d - direction) 
	(me ?x - agent) 
	(commits ?x - agent ?ac - act)
)

(:action turn_to
 :parameters ( ?s - agent ?d_new - direction ?d_prev - direction)
 :precondition
	(and (pointing ?s ?d_prev)) 
 :effect
	(and (pointing ?s ?d_new) (not (pointing ?s ?d_prev))))

(:action switch_on
 :parameters ( ?i - instrument ?s - agent)
 :precondition
	(and (on_board ?i ?s) (power_avail ?s))
 :effect
	(and (power_on ?i) (not (calibrated ?i)) (not (power_avail ?s))))

(:action calibrate
 :parameters ( ?s - agent ?i - instrument ?d - direction)
 :precondition
	(and (on_board ?i ?s) (calibration_target ?i ?d) (pointing ?s ?d) (power_on ?i))
 :effect
	 (calibrated ?i))

(:action take_image
 :parameters ( ?s - agent ?d - direction ?i - instrument ?m - mode)
 :precondition
	(and  (me ?s) (calibrated ?i) (on_board ?i ?s) (supports ?i ?m) (power_on ?i) (pointing ?s ?d))
 :effect
	 (have_image ?d ?m))

(:action request
:communication
:parameters ( ?a - agent ?b - agent ?ac - act)
:precondition (and (not (= ?a ?b))
(me ?a)
)
:observe (commits ?b ?ac)
)

)
