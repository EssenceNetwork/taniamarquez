(define (problem strips-sat-x-1)
(:domain satellite)
(:objects
	agent0 agent1 - agent
	instrument0 instrument2 - instrument
	infrared1 - mode
	Star0 Star6 - direction
)
(:init
	(supports instrument0 infrared1)
	(calibration_target instrument0 Star0)
	(on_board instrument0 agent0)
	(power_avail agent0)
	(pointing agent0 Star6)
	(supports instrument2 infrared1)
	(calibration_target instrument2 Star6)
	(on_board instrument2 agent1)
	(power_avail agent1)
	(pointing agent1 Star0)
	(me agent0)

	(unknown (commits agent1 act))
)
(:goal (and
	(have_image Star0 infrared1)
))

)
